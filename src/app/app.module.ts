import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatSelectModule,
  MatSidenavModule,
  MatTableModule,
  MatToolbarModule
} from '@angular/material';
import {TruckListComponent} from './trucks/truck-list/truck-list.component';
import {TruckEditDialogComponent} from './trucks/truck-edit-dialog/truck-edit-dialog.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ApiModule, BASE_PATH} from './rest';
import {HttpClientModule} from '@angular/common/http';
import {ConfirmDialogComponent} from './common/confirm-dialog/confirm-dialog.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';

@NgModule({
  declarations: [
    AppComponent,
    TruckListComponent,
    TruckEditDialogComponent,
    ConfirmDialogComponent
  ],
  entryComponents: [TruckEditDialogComponent, ConfirmDialogComponent],
  imports: [
    ApiModule,
    MatSnackBarModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatTableModule,
    MatToolbarModule,
    MatDialogModule,
    MatSelectModule,
    FormsModule,
    MatInputModule,
    ReactiveFormsModule
  ],
  providers: [{provide: BASE_PATH, useValue: 'http://localhost:8080/v1'}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
