import {Injectable} from '@angular/core';
import {ApplicationsegmentsService, ColorsService, FueltypesService, RangesService} from '../rest';

@Injectable({
  providedIn: 'root'
})
export class DictionariesService {

  private colors: string[];
  private applicationSegments: string[];
  private fuelTypes: string[];
  private ranges: string[];

  constructor(
    colorsService: ColorsService,
    applicationSegmentsService: ApplicationsegmentsService,
    fuelTypesService: FueltypesService,
    rangesService: RangesService
  ) {
    colorsService.getColors().subscribe(r => {
      this.colors = r;
    });
    applicationSegmentsService.getApplicationSegments().subscribe(r => {
      this.applicationSegments = r;
    });
    fuelTypesService.getFuelTypes().subscribe(r => {
      this.fuelTypes = r;
    });
    rangesService.getRanges().subscribe(r => {
      this.ranges = r;
    });
  }

  get Colors(): readonly string[] {
    return this.colors;
  }

  get ApplicationSegments(): readonly string[] {
    return this.applicationSegments;
  }

  get FuelTypes(): readonly string[] {
    return this.fuelTypes;
  }

  get Ranges(): readonly string[] {
    return this.ranges;
  }
}
