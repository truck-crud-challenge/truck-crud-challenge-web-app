import {Component, Inject, OnInit} from '@angular/core';
import {Truck} from '../../rest';
import {MAT_DIALOG_DATA} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DictionariesService} from '../../common/dictionaries.service';

@Component({
  selector: 'app-truck-edit-dialog',
  templateUrl: './truck-edit-dialog.component.html',
  styleUrls: ['./truck-edit-dialog.component.scss']
})
export class TruckEditDialogComponent implements OnInit {
  formGroup: FormGroup;

  constructor(
    private dictionariesService: DictionariesService,
    @Inject(MAT_DIALOG_DATA) private data: TruckDialogData, private formBuilder: FormBuilder) {
    if (!data.value) {
      data.value = {
        name: null,
        horsepower: null,
        displacement: null,
        fueltype: null,
        range: null,
        colors: [],
        applicationSegments: []
      };
    }
  }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      name: [this.data.value.name, Validators.required],
      horsepower: [this.data.value.horsepower, Validators.required],
      displacement: [this.data.value.displacement, Validators.required],
      range: [this.data.value.range, Validators.required],
      fueltype: [this.data.value.fueltype, Validators.required],
      colors: [this.data.value.colors, Validators.required],
      applicationSegments: [this.data.value.applicationSegments, Validators.required]
    });
  }
}

export interface TruckDialogData {
  value?: Truck;
  title: string;
  saveButton: string;
}
