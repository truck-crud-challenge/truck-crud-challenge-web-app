import {Component, OnInit, ViewChild} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {Truck, TrucksService} from '../../rest';
import {MatDialog, MatSnackBar, MatTable} from '@angular/material';
import {TruckDialogData, TruckEditDialogComponent} from '../truck-edit-dialog/truck-edit-dialog.component';
import {ConfirmDialogComponent} from '../../common/confirm-dialog/confirm-dialog.component';
import {Observable} from 'rxjs';


@Component({
  selector: 'app-truck-list',
  templateUrl: './truck-list.component.html',
  styleUrls: ['./truck-list.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ])]
})
export class TruckListComponent implements OnInit {
  trucks: Truck[];
  columnsToDisplay = ['name', 'range', 'horsepower'];
  expandedElement: Truck | null;
  @ViewChild(MatTable, {static: false})
  table: MatTable<any>;

  constructor(public dialog: MatDialog, private trucksService: TrucksService, private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.trucksService.getTrucks().subscribe(trucks => {
      this.trucks = trucks;
    });
  }

  editTruck(truck): void {
    this.openEditTruckDialog({
      value: JSON.parse(JSON.stringify(truck)),
      title: 'Edit truck',
      saveButton: 'Save'
    }).subscribe(truckToUpdate => {
      if (truckToUpdate) {
        this.trucksService.updateTruck(truck.id, truckToUpdate).subscribe(updatedTruck => {
          Object.assign(truck, updatedTruck);
          this.openConfirmationSnackBar('Truck updated');
        }, e => this.openErrorSnackBar('updating truck', e.error.message, e.error.status));
      }
    });
  }

  addNewTruck(): void {
    this.openEditTruckDialog({
      title: 'Add new truck',
      saveButton: 'Add'
    }).subscribe(truck => {
      if (truck) {
        this.trucksService.createTruck(truck).subscribe(createdTruck => {
          this.trucks.push(createdTruck);
          this.table.renderRows();
          this.openConfirmationSnackBar('Truck created');
        }, e => this.openErrorSnackBar('creating truck', e.error.message, e.error.status));
      }
    });
  }

  private openEditTruckDialog(dialogData: TruckDialogData): Observable<Truck | undefined> {
    const dialogRef = this.dialog.open(TruckEditDialogComponent, {
      width: '500px',
      data: dialogData
    });
    return dialogRef.afterClosed();
  }

  deleteTruck(truck: Truck): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      data: {
        title: 'Delete truck',
        message: 'Are you sure?'
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.trucksService.deleteTruck(truck.id).subscribe(r => {
          this.trucks = this.trucks.filter(e => e.id !== truck.id);
          this.openConfirmationSnackBar('Truck deleted');
        }, r => this.openErrorSnackBar('deleting truck', r.error.message, r.error.status));
      }
    });
  }


  private openErrorSnackBar(message: string, errorMessage: string, errorStatus: string) {
    this.snackBar.open('Error('.concat(errorStatus, ') ', message, ': ', errorMessage), 'Ok', {
      horizontalPosition: 'right'
    });
  }

  private openConfirmationSnackBar(message: string) {
    this.snackBar.open(message, null, {
      duration: 4000,
      horizontalPosition: 'right'
    });
  }
}
